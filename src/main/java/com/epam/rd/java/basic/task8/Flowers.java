package com.epam.rd.java.basic.task8;

import java.util.Comparator;

public class Flowers implements Comparator {

    public String name;
    public String soil;
    public String origin;
    public String stemColour;
    public String leafColour;
    public String aveLenFlowerMeasure;
    public String aveLenFlower;
    public String tempretureMeasure;
    public String tempreture;
    public String lightRequiring;
    public String wateringMeasure;
    public String watering;
    public String multiplying;

    @Override
    public int compare(Object o1, Object o2) {
        Flowers f1 = (Flowers)o1;
        Flowers f2 = (Flowers)o2;
        return f1.name.compareTo(f2.name);
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlowerMeasure='" + aveLenFlowerMeasure + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", tempretureMeasure='" + tempretureMeasure + '\'' +
                ", tempreture='" + tempreture + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", watering='" + watering + '\'' +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
