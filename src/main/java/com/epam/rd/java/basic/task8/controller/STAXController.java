package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.helpers.DefaultHandler;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Flowers> list = new ArrayList<>();
	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flowers> readXML(){
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			Flowers flower = null;
			int c=0;
			while (reader.hasNext()) {

				XMLEvent xmlEvent = reader.nextEvent();
				if(xmlEvent.isStartElement()){
					StartElement startElement = xmlEvent.asStartElement();
					if(startElement.getName().getLocalPart().equals("flower"))
					{
						flower = new Flowers();
					}
					switch(startElement.getName().getLocalPart()){
						case "name":
							xmlEvent=reader.nextEvent();
							flower.name = xmlEvent.asCharacters().getData();
							break;
						case "soil":
							xmlEvent=reader.nextEvent();
							flower.soil = xmlEvent.asCharacters().getData();
							break;
						case "origin":
							xmlEvent=reader.nextEvent();
							flower.origin = xmlEvent.asCharacters().getData();
							break;
						case "stemColour":
							xmlEvent=reader.nextEvent();
							flower.stemColour = xmlEvent.asCharacters().getData();
							break;
						case "leafColour":
							xmlEvent=reader.nextEvent();
							flower.leafColour = xmlEvent.asCharacters().getData();
							break;
						case "aveLenFlower":
							xmlEvent=reader.nextEvent();
							Attribute measure = startElement.getAttributeByName(new QName("measure"));
							flower.aveLenFlowerMeasure = measure.getValue();
							flower.aveLenFlower = xmlEvent.asCharacters().getData();
							break;
						case "tempreture":
							xmlEvent=reader.nextEvent();
							Attribute measure1 = startElement.getAttributeByName(new QName("measure"));
							flower.tempretureMeasure = measure1.getValue();
							flower.tempreture = xmlEvent.asCharacters().getData();
							break;
						case "watering":
							xmlEvent=reader.nextEvent();
							Attribute measure2 = startElement.getAttributeByName(new QName("measure"));
							flower.wateringMeasure = measure2.getValue();
							flower.watering = xmlEvent.asCharacters().getData();
							break;
						case "lighting":
							xmlEvent=reader.nextEvent();
							Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
							flower.lightRequiring = lightRequiring.getValue();
							break;
						case "multiplying":
							xmlEvent=reader.nextEvent();
							flower.multiplying = xmlEvent.asCharacters().getData();
							break;
					}
				}
				if(xmlEvent.isEndElement()){
					EndElement endElement = xmlEvent.asEndElement();
					if(endElement.getName().getLocalPart().equals("flower")){
						c++;
						list.add(flower);
					}
				}
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}
	public void writeXML(String nameFile,List<Flowers> list) throws FileNotFoundException, XMLStreamException {
		SAXController cont = new SAXController(xmlFileName);
		cont.writeXML(nameFile,list);

	}
}