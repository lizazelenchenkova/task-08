package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flowers;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	private List<Flowers> list = new ArrayList<>();
	public List<Flowers> readXML() throws ParserConfigurationException, IOException, SAXException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();


		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.parse(new File(xmlFileName));


		NodeList flowersList = document.getDocumentElement().getElementsByTagName("flower");

		for(int i=0; i< flowersList.getLength();i++){
			if(flowersList.item(i) instanceof Element){
				Node flower = flowersList.item(i);
				Flowers f = new Flowers();

				NodeList flowerChild = flower.getChildNodes();
				for(int j=0;j<flowerChild.getLength();j++){
					if(flowerChild.item(j) instanceof  Element){
						Node flowerNode = flowerChild.item(j);
						switch(flowerNode.getNodeName()){
							case "name": f.name=flowerNode.getTextContent();break;
							case "soil": f.soil=flowerNode.getTextContent();break;
							case "origin": f.origin=flowerNode.getTextContent();break;
							case "multiplying": f.multiplying=flowerNode.getTextContent();break;
							case "visualParameters":
								NodeList visualParametersList = flowerNode.getChildNodes();
								for(int k=0;k< visualParametersList.getLength();k++){
									if(visualParametersList.item(k) instanceof Element){
										Node visualParameterNode = visualParametersList.item(k);
										switch(visualParameterNode.getNodeName()){
											case "stemColour": f.stemColour=visualParameterNode.getTextContent();break;
											case "leafColour": f.leafColour=visualParameterNode.getTextContent();break;
											case "aveLenFlower":
												f.aveLenFlower=visualParameterNode.getTextContent();
												f.aveLenFlowerMeasure = visualParameterNode.getAttributes().item(0).getTextContent();
												break;

										}
									}
								}
								break;
							case "growingTips":
								NodeList growingTipsList = flowerNode.getChildNodes();
								for(int k=0;k< growingTipsList.getLength();k++){
									if(growingTipsList.item(k) instanceof Element){
										Node growingTipNode = growingTipsList.item(k);
										switch(growingTipNode.getNodeName()){
											case "tempreture":
												f.tempreture=growingTipNode.getTextContent();
												f.tempretureMeasure=growingTipNode.getAttributes().item(0).getTextContent();
												break;
											case "lighting":
												f.lightRequiring=growingTipNode.getAttributes().item(0).getTextContent();
												break;
											case "watering":
												f.watering=growingTipNode.getTextContent();
												f.wateringMeasure = growingTipNode.getAttributes().item(0).getTextContent();
												break;

										}
									}
								}
								break;
						}
					}
				}
				list.add(f);
			}
		}
		return list;
	}
	public void writeXML(String nameFile,List<Flowers> list){
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			Element rootElement = doc.createElement("flowers");
			doc.setXmlStandalone(true);
			rootElement.setAttribute("xmlns","http://www.nure.ua");
			rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			rootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
			doc.appendChild(rootElement);

			for(Flowers flower:list){
				Element flowerElement = doc.createElement("flower");
				rootElement.appendChild(flowerElement);

				Element nameElement = doc.createElement("name");
				nameElement.setTextContent(flower.name);
				flowerElement.appendChild(nameElement);

				Element soilElement = doc.createElement("soil");
				soilElement.setTextContent(flower.soil);
				flowerElement.appendChild(soilElement);

				Element originElement = doc.createElement("origin");
				originElement.setTextContent(flower.origin);
				flowerElement.appendChild(originElement);

				Element visualParametersElement = doc.createElement("visualParameters");
				flowerElement.appendChild(visualParametersElement);
				Element stemColourElement = doc.createElement("stemColour");
				stemColourElement.setTextContent(flower.stemColour);
				visualParametersElement.appendChild(stemColourElement);
				Element leafColourElement = doc.createElement("leafColour");
				leafColourElement.setTextContent(flower.leafColour);
				visualParametersElement.appendChild(leafColourElement);
				Element aveLenFlowerElement = doc.createElement("aveLenFlower");
				aveLenFlowerElement.setTextContent(flower.aveLenFlower);
				aveLenFlowerElement.setAttribute("measure",flower.aveLenFlowerMeasure);
				visualParametersElement.appendChild(aveLenFlowerElement);

				Element growingTipsElement = doc.createElement("growingTips");
				flowerElement.appendChild(growingTipsElement);
				Element tempretureElement = doc.createElement("tempreture");
				Element lightingElement = doc.createElement("lighting");
				Element wateringElement = doc.createElement("watering");
				growingTipsElement.appendChild(tempretureElement);
				growingTipsElement.appendChild(lightingElement);
				growingTipsElement.appendChild(wateringElement);
				tempretureElement.setAttribute("measure", flower.tempretureMeasure);
				tempretureElement.setTextContent(flower.tempreture);
				lightingElement.setAttribute("lightRequiring",flower.lightRequiring);
				wateringElement.setAttribute("measure", flower.wateringMeasure);
				wateringElement.setTextContent(flower.watering);

				Element multiplyingElement = doc.createElement("multiplying");
				multiplyingElement.setTextContent(flower.multiplying);
				flowerElement.appendChild(multiplyingElement);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(nameFile));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}


	}
}
